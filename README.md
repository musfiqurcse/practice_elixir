# My FirstElixir Program #
Hello Guys! It's my program relating functional programming. I am going to updated all my necessary codes in this repository.  I hope if you find any kinds of solution from here. It would be better for all of us

### What is Elixir? ###
     Elixir is a functional programming language. It is build with Erlang Virtual Machines where it can concurrently process a lots of operation from different cores

### How do I get set up? ###

* Go to [Erlang](https://www.erlang.org/downloads) official sites you can find the documentation there.
* After configuration, Install the [Elixir](http://elixir-lang.org/install.html) what you can find in the official sites 
* To know more about a web framework, You can visit [**_Phoenix Framework_**](http://www.phoenixframework.org/)
